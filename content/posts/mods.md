---
title: "Minecraft 1.12.2 Mods"
date: 2020-06-16T23:26:34+08:00
draft: false
---

## Mod list ([Download all files](../../resource/Minecraft_1.12.2_Mods.tar.gz))
 - [[1.12]bspkrsCore-universal-7.6.0.1.jar](../../resource/mods/[1.12]bspkrsCore-universal-7.6.0.1.jar)
 - [[1.12]TreeCapitator-client-1.43.0.jar](../../resource/mods/[1.12]TreeCapitator-client-1.43.0.jar')
 - [AppleSkin-mc1.12-1.0.9.jar](../../resource/mods/AppleSkin-mc1.12-1.0.9.jar)
 - [BetterBuildersWands-1.12-0.11.1.245+69d0d70.jar](../../resource/mods/BetterBuildersWands-1.12-0.11.1.245+69d0d70.jar)
 - [BetterFps-1.4.8.jar](../../resource/mods/BetterFps-1.4.8.jar)
 - [buildcraft-all-7.99.24.6.jar](../../resource/mods/buildcraft-all-7.99.24.6.jar)
 - [camera-1.0.8.jar](../../resource/mods/camera-1.0.8.jar)
 - [ChickenASM-1.12-1.0.2.7.jar](../../resource/mods/ChickenASM-1.12-1.0.2.7.jar)
 - [CodeChickenLib-1.12.2-3.2.3.358-universal.jar](../../resource/mods/CodeChickenLib-1.12.2-3.2.3.358-universal.jar)
 - [ExtraPlanets-1.12.2-0.5.8.jar](../../resource/mods/ExtraPlanets-1.12.2-0.5.8.jar)
 - [FastLeafDecay-v14.jar](../../resource/mods/FastLeafDecay-v14.jar)
 - [ForgeMultipart-1.12.2-2.6.2.83-universal.jar](../../resource/mods/ForgeMultipart-1.12.2-2.6.2.83-universal.jar)
 - [GalacticraftCore-1.12.2-4.0.2.238.jar](../../resource/mods/GalacticraftCore-1.12.2-4.0.2.238.jar)
 - [Galacticraft-Planets-1.12.2-4.0.2.238.jar](../../resource/mods/Galacticraft-Planets-1.12.2-4.0.2.238.jar)
 - [industrialcraft-2-2.8.170-ex112-tw.jar](../../resource/mods/industrialcraft-2-2.8.170-ex112-tw.jar)
 - [InventoryTweaks-1.63.jar](../../resource/mods/InventoryTweaks-1.63.jar)
 - [jei_1.12.2-4.15.0.291.jar](../../resource/mods/jei_1.12.2-4.15.0.291.jar)
 - [journeymap-1.12.2-5.5.7.jar](../../resource/mods/journeymap-1.12.2-5.5.7.jar)
 - [Mekanism-1.12.2-9.8.3.390.jar](../../resource/mods/Mekanism-1.12.2-9.8.3.390.jar)
 - [MekanismGenerators-1.12.2-9.8.3.390.jar](../../resource/mods/MekanismGenerators-1.12.2-9.8.3.390.jar)
 - [MekanismTools-1.12.2-9.8.3.390.jar](../../resource/mods/MekanismTools-1.12.2-9.8.3.390.jar)
 - [MicdoodleCore-1.12.2-4.0.2.238.jar](../../resource/mods/MicdoodleCore-1.12.2-4.0.2.238.jar)
 - [MJRLegendsLib-1.12.2-1.1.9.jar](../../resource/mods/MJRLegendsLib-1.12.2-1.1.9.jar)
 - [MrTJPCore-1.12.2-2.1.4.43-universal.jar](../../resource/mods/MrTJPCore-1.12.2-2.1.4.43-universal.jar)
 - [Neat+1.4-17.jar](../../resource/mods/Neat+1.4-17.jar)
 - [ProjectRed-1.12.2-4.9.4.120-Base.jar](../../resource/mods/ProjectRed-1.12.2-4.9.4.120-Base.jar)
 - [ProjectRed-1.12.2-4.9.4.120-fabrication.jar](../../resource/mods/ProjectRed-1.12.2-4.9.4.120-fabrication.jar)
 - [ProjectRed-1.12.2-4.9.4.120-integration.jar](../../resource/mods/ProjectRed-1.12.2-4.9.4.120-integration.jar)
 - [ProjectRed-1.12.2-4.9.4.120-lighting.jar](../../resource/mods/ProjectRed-1.12.2-4.9.4.120-lighting.jar)
 - [ProjectRed-1.12.2-4.9.4.120-mechanical.jar](../../resource/mods/ProjectRed-1.12.2-4.9.4.120-mechanical.jar)
 - [ProjectRed-1.12.2-4.9.4.120-world.jar](../../resource/mods/ProjectRed-1.12.2-4.9.4.120-world.jar)

## Shader Mod & Pack
 - [OptiFine_1.12.2_HD_U_F5_MOD.jar](../../resource/shaders/OptiFine_1.12.2_HD_U_F5_MOD.jar)
 - [projectLUMA-v1.32.zip](../../resource/shaders/projectLUMA-v1.32.zip)
